<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="scene_objects" 
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Data pipelines</title>

  <para>
    Data pipelines are the central concept of OVITO. You can find a general introduction to 
    this concept in the <link linkend="usage.modification_pipeline">first part</link> of this manual. 
    The following reference sections list the different kinds of building blocks that you will
    need to create data pipelines in OVITO:
    <itemizedlist>
      <listitem><para><link linkend="pipelines.data_sources">Data sources</link></para></listitem>
      <listitem><para><link linkend="pipelines.data_objects">Data objects</link></para></listitem>
      <listitem><para><link linkend="particles.modifiers">Modifiers</link></para></listitem>
      <listitem><para><link linkend="display_objects">Visual elements</link></para></listitem>
    </itemizedlist>
  </para>

  <simplesect xml:id="pipelines.data_sources">
    <title>Data sources</title>
    <para>
      <informalfigure><screenshot><mediaobject><imageobject>
        <imagedata fileref="images/scene_objects/data_source_and_data_objects.svg" format="SVG" scale="50" />
      </imageobject></mediaobject></screenshot></informalfigure>

      A <emphasis>data source</emphasis> holds, provides or generates the input data 
      for a <link linkend="usage.modification_pipeline">data pipeline</link>. In the pipeline editor depicted 
      in the screenshot, the current data source appears under the "Data source" section.
      The type of source you will work with most of the time is the <link linkend="scene_objects.file_source">External file</link>
      source, which loads the input data for the pipeline from a file stored on your local computer or a remote machine.
    </para>

    <para>
      <informaltable>
        <tgroup cols="2">
          <colspec colname='name'/>
          <colspec colname='descr'/>
          <thead>
            <row>
              <entry>Data&#xA0;source</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>          
            <row>
              <entry><link linkend="scene_objects.file_source">External&#xA0;file</link></entry>
              <entry>Reads data from external file and hands it to the pipeline</entry>
            </row>
            <row>
              <entry>Static&#xA0;source</entry>
              <entry>Generic source holding one or more static data objects</entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>
    </para>
  </simplesect>

  <simplesect xml:id="pipelines.data_objects">
    <title>Data objects</title>

    <para>
      A dataset loaded from a file typically consists of several <emphasis>data objects</emphasis>, which represent different
      facets of the information. Data object are the entities which get processed by <link link="usage.modification_pipeline">modifiers</link> while flowing 
      through the data pipeline. Some of the data objects loaded from an input file, but not all, appear as sub-entries of the 
      data source in the pipeline editor (see screenshot).
    </para>
    
    <para>
      <informaltable>
        <tgroup cols="2">
          <colspec colname='name'/>
          <colspec colname='descr'/>
          <thead>
            <row>
              <entry>Data&#xA0;object</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry>Property</entry>
              <entry>A regular property of particle, bonds, voxels or other elements</entry>
            </row>
            <row>
              <entry><link linkend="scene_objects.particle_types">Particle&#xA0;types</link>,
                <link linkend="scene_objects.particle_types">Bond&#xA0;types</link>,
                <link linkend="scene_objects.particle_types">Molecule&#xA0;types</link></entry>
              <entry>Typed property letting you change the names, display colors and radii of the defined types</entry>
            </row>
            <row>
              <entry><link linkend="scene_objects.simulation_cell">Simulation&#xA0;cell</link></entry>
              <entry>The simulation cell geometry and boundary conditions</entry>
            </row>
            <row>
              <entry><link linkend="scene_objects.surface_mesh">Surface&#xA0;mesh</link></entry>
              <entry>Mesh representing a two-dimensional closed manifold embedded in the simulation domain</entry>
            </row>
            <row>
              <entry>Triangle&#xA0;mesh</entry>
              <entry>A general mesh made of triangle elements</entry>
            </row>
            <row>
              <entry>Camera</entry>
              <entry>A viewport camera object created using the <link linkend="usage.viewports.menu">Create camera</link> function in the viewport menu</entry>
            </row>
            <row>
              <entry>Particle&#xA0;trajectories</entry>
              <entry>The output of the <link linkend="particles.modifiers.generate_trajectory_lines">Generate trajectory lines</link> modifier</entry>
            </row>
            <row>
              <entry>Voxel&#xA0;grid</entry>
              <entry>A structured grid of voxel elements</entry>
            </row>
            <row>
              <entry>Dislocations</entry>
              <entry>Lines generated by the <link linkend="particles.modifiers.dislocation_analysis">Dislocation analysis (DXA)</link> modifier</entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>
    </para>
  </simplesect> 

  <simplesect>
    <title>See also</title>
    <para>
      <link xlink:href="python/modules/ovito_data.html#ovito.data.DataObject"><classname>DataObject</classname> (Python API)</link>
    </para>
  </simplesect> 

  <xi:include href="file_source.docbook"/>
  <xi:include href="particle_types.docbook"/>
  <xi:include href="simulation_cell.docbook"/>
  <xi:include href="surface_mesh.docbook"/>
  <xi:include href="clone_pipeline.docbook"/>
  
</section>
